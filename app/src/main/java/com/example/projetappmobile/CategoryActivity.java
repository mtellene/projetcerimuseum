package com.example.projetappmobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

public class CategoryActivity extends AppCompatActivity {

    private RecyclerView rvPeriph, rvStationTravail, rvComposant, rvOrdi, rvEcran, rvCableAdapteur, rvSCSI, rvOrdiPoche, rvOrdiPortable, rvTelephone, rv8Bits, rvReseau, rvTerminalCom, rvPrototype, rvRegleCalcul, rvOrdiBureau, rvSuppStockage;

    private ArtefactDbHelper dbHelper;

    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = findViewById(R.id.toolbar);

        dbHelper = new ArtefactDbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //recyclerView categorie station de travail
        rvStationTravail = (RecyclerView) findViewById(R.id.recyclerViewStationTravail);
        rvStationTravail.setLayoutManager(new LinearLayoutManager(this));
        rvStationTravail.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvStationTravail.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cursorStationTravail = dbHelper.fetchArtefactsByCat("StationTravail");
        adapter = new MyAdapter(CategoryActivity.this, cursorStationTravail);
        rvStationTravail.setAdapter(adapter);

        //recyclerView categorie ordinateur
        rvOrdi = (RecyclerView) findViewById(R.id.recyclerViewOrdinateur);
        rvOrdi.setLayoutManager(new LinearLayoutManager(this));
        rvOrdi.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvOrdi.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cursorOrdinateur = dbHelper.fetchArtefactsByCat("Ordinateur");
        adapter = new MyAdapter(CategoryActivity.this, cursorOrdinateur);
        rvOrdi.setAdapter(adapter);

        //recyclerView categorie ordianteur portable
        rvOrdiPortable = (RecyclerView) findViewById(R.id.recyclerViewOrdinateurPortable);
        rvOrdiPortable.setLayoutManager(new LinearLayoutManager(this));
        rvOrdiPortable.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvOrdiPortable.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cursorOrdinateurPortable = dbHelper.fetchArtefactsByCat("OrdinateurPortable");
        adapter = new MyAdapter(CategoryActivity.this, cursorOrdinateurPortable);
        rvOrdiPortable.setAdapter(adapter);

        //recyclerView categorie terminal de commande
        rvTerminalCom = (RecyclerView) findViewById(R.id.recyclerViewTerminalCom);
        rvTerminalCom.setLayoutManager(new LinearLayoutManager(this));
        rvTerminalCom.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvTerminalCom.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cursorTermCom = dbHelper.fetchArtefactsByCat("TerminalCom");
        adapter = new MyAdapter(CategoryActivity.this, cursorTermCom);
        rvTerminalCom.setAdapter(adapter);

        //recyclerView categorie périphérique
        rvPeriph = (RecyclerView) findViewById(R.id.recyclerViewPeriph);
        rvPeriph.setLayoutManager(new LinearLayoutManager(this));
        rvPeriph.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvPeriph.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorPeriph = dbHelper.fetchArtefactsByCat("Periph");
        adapter = new MyAdapter(CategoryActivity.this, cusorPeriph);
        rvPeriph.setAdapter(adapter);

        //recyclerView categorie composant
        rvComposant = (RecyclerView) findViewById(R.id.recyclerViewCompo);
        rvComposant.setLayoutManager(new LinearLayoutManager(this));
        rvComposant.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvComposant.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorCompo = dbHelper.fetchArtefactsByCat("Composant");
        adapter = new MyAdapter(CategoryActivity.this, cusorCompo);
        rvComposant.setAdapter(adapter);

        //recyclerView categorie support de stockage
        rvSuppStockage = (RecyclerView) findViewById(R.id.recyclerViewSuppStockage);
        rvSuppStockage.setLayoutManager(new LinearLayoutManager(this));
        rvSuppStockage.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvSuppStockage.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorSuppStockage = dbHelper.fetchArtefactsByCat("SupportStockage");
        adapter = new MyAdapter(CategoryActivity.this, cusorSuppStockage);
        rvSuppStockage.setAdapter(adapter);

        //recyclerView categorie ordinateur de poche
        rvOrdiPoche = (RecyclerView) findViewById(R.id.recyclerViewOrdinateurPoche);
        rvOrdiPoche.setLayoutManager(new LinearLayoutManager(this));
        rvOrdiPoche.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvOrdiPoche.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorOrdiPoche = dbHelper.fetchArtefactsByCat("OrdinateurPoche");
        adapter = new MyAdapter(CategoryActivity.this, cusorOrdiPoche);
        rvOrdiPoche.setAdapter(adapter);

        //recyclerView categorie prototype
        rvPrototype = (RecyclerView) findViewById(R.id.recyclerViewProto);
        rvPrototype.setLayoutManager(new LinearLayoutManager(this));
        rvPrototype.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvPrototype.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorProto = dbHelper.fetchArtefactsByCat("Prototype");
        adapter = new MyAdapter(CategoryActivity.this, cusorProto);
        rvPrototype.setAdapter(adapter);

        //recyclerView categorie ordinateur de bureau
        rvOrdiBureau = (RecyclerView) findViewById(R.id.recyclerViewOrdiBureau);
        rvOrdiBureau.setLayoutManager(new LinearLayoutManager(this));
        rvOrdiBureau.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvOrdiBureau.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorOrdiBureau = dbHelper.fetchArtefactsByCat("OrdinateurBureau");
        adapter = new MyAdapter(CategoryActivity.this, cusorOrdiBureau);
        rvOrdiBureau.setAdapter(adapter);

        //recyclerView categorie SCSI
        rvSCSI = (RecyclerView) findViewById(R.id.recyclerViewSCSI);
        rvSCSI.setLayoutManager(new LinearLayoutManager(this));
        rvSCSI.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvSCSI.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorSCSI = dbHelper.fetchArtefactsByCat("SCSI");
        adapter = new MyAdapter(CategoryActivity.this, cusorSCSI);
        rvSCSI.setAdapter(adapter);

        //recyclerView categorie téléphone
        rvTelephone = (RecyclerView) findViewById(R.id.recyclerViewTelephone);
        rvTelephone.setLayoutManager(new LinearLayoutManager(this));
        rvTelephone.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvTelephone.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorTelephone = dbHelper.fetchArtefactsByCat("Telephone");
        adapter = new MyAdapter(CategoryActivity.this, cusorTelephone);
        rvTelephone.setAdapter(adapter);

        //recyclerView categorie règle à calcul
        rvRegleCalcul = (RecyclerView) findViewById(R.id.recyclerRegleCalcul);
        rvRegleCalcul.setLayoutManager(new LinearLayoutManager(this));
        rvRegleCalcul.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvRegleCalcul.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorRegleCalcul = dbHelper.fetchArtefactsByCat("RegleCalcul");
        adapter = new MyAdapter(CategoryActivity.this, cusorRegleCalcul);
        rvRegleCalcul.setAdapter(adapter);

        //recyclerView categorie écran
        rvEcran = (RecyclerView) findViewById(R.id.recyclerEcran);
        rvEcran.setLayoutManager(new LinearLayoutManager(this));
        rvEcran.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvEcran.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorEcran = dbHelper.fetchArtefactsByCat("Ecran");
        adapter = new MyAdapter(CategoryActivity.this, cusorEcran);
        rvEcran.setAdapter(adapter);

        //recyclerView categorie 8 bits
        rv8Bits = (RecyclerView) findViewById(R.id.recycler8Bits);
        rv8Bits.setLayoutManager(new LinearLayoutManager(this));
        rv8Bits.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rv8Bits.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusor8Bits = dbHelper.fetchArtefactsByCat("Bits");
        adapter = new MyAdapter(CategoryActivity.this, cusor8Bits);
        rv8Bits.setAdapter(adapter);

        //recyclerView categorie cables adaptateurs
        rvCableAdapteur = (RecyclerView) findViewById(R.id.recyclerViewCableAdaptateur);
        rvCableAdapteur.setLayoutManager(new LinearLayoutManager(this));
        rvCableAdapteur.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvCableAdapteur.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorCableAdaptateur = dbHelper.fetchArtefactsByCat("CablesAdaptateurs");
        adapter = new MyAdapter(CategoryActivity.this, cusorCableAdaptateur);
        rvCableAdapteur.setAdapter(adapter);

        //recyclerView categorie reseau
        rvReseau = (RecyclerView) findViewById(R.id.recyclerViewRes);
        rvReseau.setLayoutManager(new LinearLayoutManager(this));
        rvReseau.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        rvReseau.setNestedScrollingEnabled(false);    //empeche le scroll
        Cursor cusorRes = dbHelper.fetchArtefactsByCat("Reseau");
        adapter = new MyAdapter(CategoryActivity.this, cusorRes);
        rvReseau.setAdapter(adapter);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    class MyAdapter extends RecyclerView.Adapter<RowHolder> {

        public CursorAdapter cursorFromDb;
        public Context context;

        public MyAdapter(Context context, Cursor cursor){
            this.context = context;
            cursorFromDb = new CursorAdapter(context, cursor, 0) {
                @Override
                public View newView(Context context, Cursor cursor, ViewGroup parent) {
                    return null;
                }

                @Override
                public void bindView(View view, Context context, Cursor cursor) {

                }
            };
        }

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.row_artefact, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            cursorFromDb.getCursor().moveToPosition(position);
            holder.bindModel(cursorFromDb.getCursor());
        }

        @Override
        public int getItemCount() {
            return (cursorFromDb.getCount());
        }

    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView nameArtefact;
        TextView catArtefact;
        ImageView img;

        private Artefact artefact;

        long rowID = 0;

        RowHolder(View row) {
            super(row);
            row.setOnClickListener(this);
            nameArtefact = (TextView) row.findViewById(R.id.nameArtefact);
            catArtefact = (TextView) row.findViewById(R.id.catArtefact);
            img = (ImageView) row.findViewById(R.id.imgArtefact);
        }

        @Override
        public void onClick(View v) {
            System.out.println(rowID);
            System.out.println(artefact.getName());
            Intent intent = new Intent(CategoryActivity.this, ArtefactActivity.class);
            intent.putExtra(Artefact.TAG, artefact);
            startActivityForResult(intent, 2);
        }

        void bindModel(Cursor cursor) {
            artefact = dbHelper.cursorToArtefact(cursor);
            rowID = artefact.getId();
            nameArtefact.setText(artefact.getName());
            catArtefact.setText(artefact.getCategory());
            File path = new File(CategoryActivity.this.getExternalFilesDir(null), cursor.getString(cursor.getColumnIndex(dbHelper.COLUMN_ARTEFACT_NAME))+".png");
            Bitmap myBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
            img.setImageBitmap(myBitmap);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
