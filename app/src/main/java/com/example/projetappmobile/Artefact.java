package com.example.projetappmobile;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;


public class Artefact implements Parcelable {

    public static final String TAG = Artefact.class.getSimpleName();

    private long id; // used for the _id colum of the db helper

    private String name;
    private String idArtefact;
    private String categories;  //array
    private String brand;
    private String timeframe;
    private String description;
    private int year;
    private String tecDetails;  //array
    private String working;
    private String pictures;    //array
    private String badge;
    private String lastUpdate;

    public Artefact(String idArtefact){ this.idArtefact = idArtefact; }

    public Artefact(long id, String name, String idArtefact, String categories, String brand, String timeframe, String description, int year, String tecDetails, String working, String badge, String lastUpdate) {
        this.id = id;
        this.name = name;
        this.idArtefact = idArtefact;
        this.categories = categories;
        this.brand = brand;
        this.timeframe = timeframe;
        this.description = description;
        this.year = year;
        this.tecDetails = tecDetails;
        this.working = working;
        this.badge = badge;
        this.lastUpdate = lastUpdate;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIdArtefact() {
        return idArtefact;
    }

    public String getCategory() { return categories; }

    public String getBrand() { return brand; }

    public String getTimeFrame() { return timeframe;}

    public String getDescription() {
        return description;
    }

    public int getYear() {
        return year;
    }

    public String getTecDetails() {
        return tecDetails;
    }

    public String getWorking() { return working; }

    public String getBadge() { return badge; }

    public String getLastUpdate() {
        return lastUpdate;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String categories) { this.categories = categories; }

    public void setBrand(String brand) { this.brand = brand; }

    public void setTimeFrame(String timeframe) { this.timeframe = timeframe;}

    public void setDescription(String description) {
        this.description = description;
    }

    public void setYear(int year) { this.year = year; }

    public void setTecDetails(String tecDetails) {
        this.tecDetails = tecDetails;
    }

    public void setWorking(String working) { this.working = working; }

    public void setBadge(String badge) { this.badge = badge; }

    public void setLastUpdate() {
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
        this.lastUpdate = dateFormat.format(currentTime);
    }

    @Override
    public String toString() {
        String aReturn = this.name + "," + this.categories + "," + this.working + "," + this.idArtefact + "," +
                this.year + "," + this.brand;
        return aReturn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(idArtefact);
        dest.writeString(categories);
        dest.writeString(brand);
        dest.writeString(timeframe);
        dest.writeString(description);
        dest.writeInt(year);
        dest.writeString(tecDetails);
        dest.writeString(working);
        dest.writeString(pictures);
        dest.writeString(badge);
        dest.writeString(lastUpdate);
    }

    public static final Creator<Artefact> CREATOR = new Creator<Artefact>()
    {
        @Override
        public Artefact createFromParcel(Parcel source)
        {
            return new Artefact(source);
        }

        @Override
        public Artefact[] newArray(int size)
        {
            return new Artefact[size];
        }
    };

    public Artefact(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.idArtefact = in.readString();
        this.categories = in.readString();
        this.brand = in.readString();
        this.timeframe = in.readString();
        this.description = in.readString();
        this.year = in.readInt();
        this.tecDetails = in.readString();
        this.working = in.readString();
        this.pictures = in.readString();
        this.badge = in.readString();
        this.lastUpdate = in.readString();
    }
}
