package com.example.projetappmobile;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    // https://demo-lia.univ-avignon.fr/cerimuseum/catalog
    private static final String SEARCH_CATALOG = "catalog";

    // Build URL to get information for all items
    public static URL buildSearchAllArtefacts() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // https://demo-lia.univ-avignon.fr/cerimuseum/items/idArtefact
    private static final String SEARCH_ITEMS = "items";

    // Build URL to get information for all items
    public static URL buildSearchPicturesArtefact(String idArtefact) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS).appendPath(idArtefact);
        URL url = new URL(builder.build().toString());
        return url;
    }

}
