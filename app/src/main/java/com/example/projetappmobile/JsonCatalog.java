package com.example.projetappmobile;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JsonCatalog {

    private ArtefactDbHelper dbHelper;

    private List<String> categories;

    public JsonCatalog(ArtefactDbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCatalog(reader);
        } finally {
            reader.close();
        }
    }

    public void readCatalog(JsonReader reader) throws IOException {
        reader.beginObject();   //asserts it is the begin of the reader
        while (reader.hasNext()) {  //hasNext() returns true if the reader has another element
            String name = reader.nextName();    //nextName() returns the next token (JsonToken)
            Artefact artefact = new Artefact(name);
            readCatalogArray(reader,artefact); //call readCatalogArray on the reader
        }
        reader.endObject(); //asserts it is the end of the reader
    }

    public void readCatalogArray(JsonReader reader, Artefact artefact) throws IOException {
        reader.beginObject();    //asserts it is the begin of the array
        categories = new ArrayList<>();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("name")) {  //recup du nom
                artefact.setName(reader.nextString());
            }else if(name.equals("timeFrame")){ //recup timeFrame
                reader.beginArray();    //timeFram c'est un array json
                StringBuilder timeframe = new StringBuilder();
                while (reader.hasNext()){
                    timeframe.append(reader.nextInt());
                    if(reader.hasNext()){ timeframe.append(" - "); }    //si y'a un truc apres je met un tiret entre les dates
                }
                artefact.setTimeFrame(timeframe.toString());
                reader.endArray();
            }else if(name.equals("technicalDetails")){  //recup tec det
                reader.beginArray();    //c'est un array json
                StringBuilder tecDet = new StringBuilder();
                while (reader.hasNext()){
                    tecDet.append("- ").append(reader.nextString());
                    if(reader.hasNext()){ tecDet.append("\n"); }
                }
                artefact.setTecDetails(tecDet.toString());
                reader.endArray();
            }else if(name.equals("categories")){
                reader.beginArray();
                StringBuilder cat = new StringBuilder();
                while (reader.hasNext()){
                    String catToTable = reader.nextString();
                    cat.append(catToTable);
                    categories.add(catToTable);
                    if(reader.hasNext()){ cat.append(" - "); }
                }
                artefact.setCategory(cat.toString());
                reader.endArray();
            }else if (name.equals("brand")) {
                artefact.setBrand(reader.nextString());
            } else if (name.equals("year")) {
                artefact.setYear(reader.nextInt());
            } else if (name.equals("description")) {
                artefact.setDescription(reader.nextString());
            } else if (name.equals("working")) {
                boolean working = reader.nextBoolean();
                if(working){
                    artefact.setWorking("oui");
                }
            } else {
                reader.skipValue();
            }
        }
        if(artefact.getWorking() == null){  //si pas de working
            artefact.setWorking("non");
        }
        if(artefact.getBrand() == null){    //si pas de brand
            artefact.setBrand("Non renseigné");
        }
        if(artefact.getTecDetails() == null){   //si pas de technical details
            artefact.setTecDetails("Non renseigné");
        }
        if(artefact.getYear() == 0){    //si pas de year -> je donne la première année de timeFrame
            String year = artefact.getTimeFrame().substring(0,4);
            artefact.setYear(Integer.parseInt(year));
        }
        //set the badge
        artefact.setBadge("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+artefact.getIdArtefact()+"/thumbnail");
        //set lastupdate
        artefact.setLastUpdate();
        //cas particuliers
        if(artefact.getIdArtefact().equals("k0j")){ //c'est l'Apple //c
            artefact.setName("Apple 2c");   //je change le nom car probleme pour le badge
        }else if(artefact.getIdArtefact().equals("jne")){ //c'est le Monitor //c
            artefact.setName("Monitor 2c");   //je change le nom car probleme pour le badge
        }else if(artefact.getIdArtefact().equals("tnx")){ //c'est le T07/70
            artefact.setName("T07 70");   //je change le nom car probleme pour le badge
        }
        dbHelper.addArtefact(artefact);
        //gestion des categories
        for(int i=0 ; i<categories.size() ; i++){
            if(categories.get(i).equals("station de travail")){
                dbHelper.addArtefactToCateoriesTable(artefact,"StationTravail");
            }else if(categories.get(i).equals("ordinateur")){
                dbHelper.addArtefactToCateoriesTable(artefact,"Ordinateur");
            }else if(categories.get(i).equals("ordinateur portable")){
                dbHelper.addArtefactToCateoriesTable(artefact,"OrdinateurPortable");
            }else if(categories.get(i).equals("terminal de communication")){
                dbHelper.addArtefactToCateoriesTable(artefact,"TerminalCom");
            }else if(categories.get(i).equals("periphérique") || categories.get(i).equals("périphérique")){
                dbHelper.addArtefactToCateoriesTable(artefact,"Periph");
            }else if(categories.get(i).equals("composant")){
                dbHelper.addArtefactToCateoriesTable(artefact,"Composant");
            }else if(categories.get(i).equals("support de stockage")){
                dbHelper.addArtefactToCateoriesTable(artefact,"SupportStockage");
            }else if(categories.get(i).equals("ordinateur de poche")){
                dbHelper.addArtefactToCateoriesTable(artefact,"OrdinateurPoche");
            }else if(categories.get(i).equals("prototype")){
                dbHelper.addArtefactToCateoriesTable(artefact,"Prototype");
            }else if(categories.get(i).equals("ordinateur de bureau")){
                dbHelper.addArtefactToCateoriesTable(artefact,"OrdinateurBureau");
            }else if(categories.get(i).equals("SCSI")){
                dbHelper.addArtefactToCateoriesTable(artefact,"SCSI");
            }else if(categories.get(i).equals("téléphone")){
                dbHelper.addArtefactToCateoriesTable(artefact,"Telephone");
            }else if(categories.get(i).equals("règle à calcul")){
                dbHelper.addArtefactToCateoriesTable(artefact,"RegleCalcul");
            }else if(categories.get(i).equals("écran")){
                dbHelper.addArtefactToCateoriesTable(artefact,"Ecran");
            }else if(categories.get(i).equals("8 bits")){
                dbHelper.addArtefactToCateoriesTable(artefact,"Bits");
            }else if(categories.get(i).equals("câble/adaptateur")){
                dbHelper.addArtefactToCateoriesTable(artefact,"CablesAdaptateurs");
            }else if(categories.get(i).equals("réseau")){
                dbHelper.addArtefactToCateoriesTable(artefact,"Reseau");
            }
        }
        reader.endObject();
    }
}
