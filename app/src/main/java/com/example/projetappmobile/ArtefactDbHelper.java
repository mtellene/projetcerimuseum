package com.example.projetappmobile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class ArtefactDbHelper extends SQLiteOpenHelper {

    private static final String TAG = ArtefactDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "artefact.db";
    //##############################################################################################
    public static final String TABLE_NAME_ARTEFACT = "artefact";
    public static final String _ID = "_id";
    public static final String COLUMN_ARTEFACT_NAME = "artefact";
    public static final String COLUMN_ARTEFACT_ID = "idArtefact";
    public static final String COLUMN_CATEGORIES = "categories";
    public static final String COLUMN_ARTEFACT_BRAND = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION = "description";
    public static final String COLUMN_ARTEFACT_YEAR = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING = "working";
    public static final String COLUMN_ARTEFACT_BADGE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE = "lastupdate";

    static final String SQL_Delete_Artefact_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_ARTEFACT;
    //##############################################################################################
    public static final String TABLE_NAME_STATION_TRAVAIL = "StationTravail";
    public static final String _ID_STATION_TRAVAIL_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_STATION_TRAVAIL_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_STATION_TRAVAIL_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_STATION_TRAVAIL_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_STATION_TRAVAIL_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_STATION_TRAVAIL_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_STATION_TRAVAIL_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_STATION_TRAVAIL_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_STATION_TRAVAIL_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_STATION_TRAVAIL_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_STATION_TRAVAIL_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_STATION_TRAVAIL_TABLE = "lastupdate";

    static final String SQL_Delete_StationTravail_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_STATION_TRAVAIL;
    //##############################################################################################
    public static final String TABLE_NAME_ORDINATEUR = "Ordinateur";
    public static final String _ID_ORDINATEUR_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_ORDINATEUR_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_ORDINATEUR_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_ORDINATEUR_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_ORDINATEUR_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_ORDINATEUR_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_ORDINATEUR_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_ORDINATEUR_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_ORDINATEUR_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_ORDINATEUR_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_ORDINATEUR_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_ORDINATEUR_TABLE = "lastupdate";

    static final String SQL_Delete_Ordinateur_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_ORDINATEUR;
    //##############################################################################################
    public static final String TABLE_NAME_ORDINATEUR_PORTABLE = "OrdinateurPortable";
    public static final String _ID_ORDINATEUR_PORTABLE_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_ORDINATEUR_PORTABLE_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_ORDINATEUR_PORTABLE_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_ORDINATEUR_PORTABLE_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_ORDINATEUR_PORTABLE_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_ORDINATEUR_PORTABLE_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_ORDINATEUR_PORTABLE_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_ORDINATEUR_PORTABLE_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_ORDINATEUR_PORTABLE_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_ORDINATEUR_PORTABLE_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_ORDINATEUR_PORTABLE_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_ORDINATEUR_PORTABLE_TABLE = "lastupdate";

    static final String SQL_Delete_OrdinateurPortable_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_ORDINATEUR_PORTABLE;
    //##############################################################################################
    public static final String TABLE_NAME_TERM_COM = "TerminalCom";
    public static final String _ID_TERM_COM_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_TERM_COM_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_TERM_COM_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_TERM_COM_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_TERM_COM_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_TERM_COM_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_TERM_COM_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_TERM_COM_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_TERM_COM_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_TERM_COM_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_TERM_COM_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_TERM_COM_TABLE = "lastupdate";

    static final String SQL_Delete_TerminalCom_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_TERM_COM;
    //##############################################################################################
    public static final String TABLE_NAME_PERIPH= "Periph";
    public static final String _ID_PERIPH_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_PERIPH_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_PERIPH_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_PERIPH_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_PERIPH_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_PERIPH_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_PERIPH_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_PERIPH_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_PERIPH_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_PERIPH_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_PERIPH_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_PERIPH_TABLE = "lastupdate";

    static final String SQL_Delete_Peripĥ_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_PERIPH;
    //##############################################################################################
    public static final String TABLE_NAME_COMPO= "Composant";
    public static final String _ID_COMPO_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_COMPO_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_COMPO_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_COMPO_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_COMPO_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_COMPO_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_COMPO_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_COMPO_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_COMPO_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_COMPO_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_COMPO_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_COMPO_TABLE = "lastupdate";

    static final String SQL_Delete_Compo_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_COMPO;
    //##############################################################################################
    public static final String TABLE_NAME_SUPP_STOCKAGE= "SupportStockage";
    public static final String _ID_SUPP_STOCKAGE_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_SUPP_STOCKAGE_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_SUPP_STOCKAGE_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_SUPP_STOCKAGE_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_SUPP_STOCKAGE_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_SUPP_STOCKAGE_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_SUPP_STOCKAGE_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_SUPP_STOCKAGE_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_SUPP_STOCKAGE_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_SUPP_STOCKAGE_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_SUPP_STOCKAGE_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_SUPP_STOCKAGE_TABLE = "lastupdate";

    static final String SQL_Delete_SuppStockage_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_SUPP_STOCKAGE;
    //##############################################################################################
    public static final String TABLE_NAME_ORDI_POCHE= "OrdinateurPoche";
    public static final String _ID_ORDI_POCHE_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_ORDI_POCHE_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_ORDI_POCHE_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_ORDI_POCHE_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_ORDI_POCHE_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_ORDI_POCHE_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_ORDI_POCHE_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_ORDI_POCHE_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_ORDI_POCHE_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_ORDI_POCHE_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_ORDI_POCHE_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_ORDI_POCHE_TABLE = "lastupdate";

    static final String SQL_Delete_OrdiPoche_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_ORDI_POCHE;
    //##############################################################################################
    public static final String TABLE_NAME_PROTOTYPE= "Prototype";
    public static final String _ID_PROTOTYPE_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_PROTOTYPE_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_PROTOTYPE_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_PROTOTYPE_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_PROTOTYPE_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_PROTOTYPE_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_PROTOTYPE_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_PROTOTYPE_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_PROTOTYPE_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_PROTOTYPE_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_PROTOTYPE_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_PROTOTYPE_TABLE = "lastupdate";

    static final String SQL_Delete_Prototype_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_PROTOTYPE;
    //##############################################################################################
    public static final String TABLE_NAME_ORDI_BUREAU= "OrdinateurBureau";
    public static final String _ID_ORDI_BUREAU_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_ORDI_BUREAU_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_ORDI_BUREAU_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_ORDI_BUREAU_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_ORDI_BUREAU_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_ORDI_BUREAU_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_ORDI_BUREAU_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_ORDI_BUREAU_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_ORDI_BUREAU_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_ORDI_BUREAU_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_ORDI_BUREAU_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_ORDI_BUREAU_TABLE = "lastupdate";

    static final String SQL_Delete_OrdiBureau_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_ORDI_BUREAU;
    //##############################################################################################
    public static final String TABLE_NAME_SCSI= "SCSI";
    public static final String _ID_SCSI_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_SCSI_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_SCSI_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_SCSI_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_SCSI_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_SCSI_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_SCSI_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_SCSI_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_SCSI_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_SCSI_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_SCSI_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_SCSI_TABLE = "lastupdate";

    static final String SQL_Delete_SCSI_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_SCSI;
    //##############################################################################################
    public static final String TABLE_NAME_PHONE= "Telephone";
    public static final String _ID_PHONE_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_PHONE_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_PHONE_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_PHONE_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_PHONE_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_PHONE_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_PHONE_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_PHONE_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_PHONE_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_PHONE_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_PHONE_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_PHONE_TABLE = "lastupdate";

    static final String SQL_Delete_Phone_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_PHONE;
    //##############################################################################################
    public static final String TABLE_NAME_REGLE_CALCUL= "RegleCalcul";
    public static final String _ID_REGLE_CALCUL_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_REGLE_CALCUL_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_REGLE_CALCUL_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_REGLE_CALCUL_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_REGLE_CALCUL_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_REGLE_CALCUL_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_REGLE_CALCUL_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_REGLE_CALCUL_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_REGLE_CALCUL_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_REGLE_CALCUL_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_REGLE_CALCUL_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_REGLE_CALCUL_TABLE = "lastupdate";

    static final String SQL_Delete_RegleCalcul_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_REGLE_CALCUL;
    //##############################################################################################
    public static final String TABLE_NAME_ECRAN= "Ecran";
    public static final String _ID_ECRAN_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_ECRAN_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_ECRAN_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_ECRAN_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_ECRAN_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_ECRAN_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_ECRAN_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_ECRAN_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_ECRAN_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_ECRAN_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_ECRAN_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_ECRAN_TABLE = "lastupdate";

    static final String SQL_Delete_Ecran_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_ECRAN;
    //##############################################################################################
    public static final String TABLE_NAME_8_BITS= "Bits";
    public static final String _ID_8_BITS_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_8_BITS_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_8_BITS_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_8_BITS_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_8_BITS_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_8_BITS_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_8_BITS_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_8_BITS_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_8_BITS_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_8_BITS_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_8_BITS_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_8_BITS_TABLE = "lastupdate";

    static final String SQL_Delete_8Bits_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_8_BITS;
    //##############################################################################################
    public static final String TABLE_NAME_CABLE_ADAPTATEUR= "CablesAdaptateurs";
    public static final String _ID_CABLE_ADAPTATEUR_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_CABLE_ADAPTATEUR_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_CABLE_ADAPTATEUR_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_CABLE_ADAPTATEUR_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_CABLE_ADAPTATEUR_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_CABLE_ADAPTATEUR_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_CABLE_ADAPTATEUR_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_CABLE_ADAPTATEUR_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_CABLE_ADAPTATEUR_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_CABLE_ADAPTATEUR_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_CABLE_ADAPTATEUR_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_CABLE_ADAPTATEUR_TABLE = "lastupdate";

    static final String SQL_Delete_CableAdaptateur_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_CABLE_ADAPTATEUR;
    //##############################################################################################
    public static final String TABLE_NAME_RESEAU= "Reseau";
    public static final String _ID_RESEAU_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_RESEAU_TABLE = "idArtefact";
    public static final String COLUMN_ARTEFACT_NAME_RESEAU_TABLE = "artefact";
    public static final String COLUMN_CATEGORIES_RESEAU_TABLE = "categories";
    public static final String COLUMN_ARTEFACT_BRAND_RESEAU_TABLE = "brand";
    public static final String COLUMN_ARTEFACT_TIMEFRAME_RESEAU_TABLE = "timeframe"; //marketing decade
    public static final String COLUMN_ARTEFACT_DESCRIPTION_RESEAU_TABLE = "description";
    public static final String COLUMN_ARTEFACT_YEAR_RESEAU_TABLE = "year";   //1rst marketing year
    public static final String COLUMN_ARTEFACT_TECDET_RESEAU_TABLE = "tecDetails";   //technical details
    public static final String COLUMN_ARTEFACT_WORKING_RESEAU_TABLE = "working";
    public static final String COLUMN_ARTEFACT_BADGE_RESEAU_TABLE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_RESEAU_TABLE = "lastupdate";

    static final String SQL_Delete_Reseau_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_RESEAU;
    //##############################################################################################
    public static final String TABLE_NAME_IMAGES= "Images";
    public static final String _ID_IMAGES_TABLE = "_id";
    public static final String COLUMN_ARTEFACT_ID_IMAGES_TABLE = "idArtefact";
    public static final String COLUMN_NAME_PICTURE_IMAGES_TABLE = "imgName";
    public static final String COLUMN_DESCRIPTION_PICTURE_IMAGES_TABLE = "imgDescription";

    static final String SQL_Images_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_IMAGES;
    //##############################################################################################
    //##############################################################################################
    public static final String TABLE_NAME_RECHERCHE = "Recherche";
    public static final String _ID_RECHERCHE_TABLE = "_id";
    public static final String COLUMN_RECHERCHE_NAME = "artefact";
    public static final String COLUMN_RECHERCHE_ID = "idArtefact";
    public static final String COLUMN_CATEGORIES_RECHERCHE_TABLE = "categories";
    public static final String COLUMN_RECHERCHE_BRAND = "brand";
    public static final String COLUMN_RECHERCHE_TIMEFRAME = "timeframe"; //marketing decade
    public static final String COLUMN_RECHERCHE_DESCRIPTION = "description";
    public static final String COLUMN_RECHERCHE_YEAR = "year";   //1rst marketing year
    public static final String COLUMN_RECHERCHE_TECDET = "tecDetails";   //technical details
    public static final String COLUMN_RECHERCHE_WORKING = "working";
    public static final String COLUMN_RECHERCHE_BADGE = "badge";   //badge for recyclerView
    public static final String COLUMN_LAST_UPDATE_RECHERCHE_TABLE = "lastupdate";

    static final String SQL_Delete_Recherche_Table = "DROP TABLE IF EXISTS " + TABLE_NAME_RECHERCHE;

    public ArtefactDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void dropImgTable(SQLiteDatabase db) {
        db.execSQL(SQL_Images_Table);
        onCreateImg(db);
    }

    public void dropRechercheTable(SQLiteDatabase db) {
        db.execSQL(SQL_Delete_Recherche_Table);
        onCreateRecherche(db);
    }

    public void onCreateImg(SQLiteDatabase db) {
        final String SQL_CREATE_IMAGES_TABLE = "CREATE TABLE " + TABLE_NAME_IMAGES+ " (" +
                _ID_IMAGES_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_IMAGES_TABLE + " TEXT, " +
                COLUMN_NAME_PICTURE_IMAGES_TABLE + " TEXT, " +
                COLUMN_DESCRIPTION_PICTURE_IMAGES_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_IMAGES_TABLE + "," + COLUMN_NAME_PICTURE_IMAGES_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_IMAGES_TABLE);
    }

    public void onCreateRecherche(SQLiteDatabase db) {
        final String SQL_CREATE_RECHERCHE_TABLE = "CREATE TABLE " + TABLE_NAME_RECHERCHE + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_RECHERCHE_NAME + " TEXT, " +
                COLUMN_RECHERCHE_ID + " TEXT, " +
                COLUMN_CATEGORIES_RECHERCHE_TABLE + " TEXT, " +
                COLUMN_RECHERCHE_BRAND + " TEXT, " +
                COLUMN_RECHERCHE_TIMEFRAME + " TEXT, " +
                COLUMN_RECHERCHE_DESCRIPTION + " TEXT, " +
                COLUMN_RECHERCHE_YEAR + " INTEGER, " +
                COLUMN_RECHERCHE_TECDET + " TEXT, " +
                COLUMN_RECHERCHE_WORKING + " TEXT, " +
                COLUMN_RECHERCHE_BADGE + " TEXT, " +
                COLUMN_LAST_UPDATE_RECHERCHE_TABLE+ " TEXT, " +

                // To assure the application have just one artefact entry per
                // artefact id, it's created a UNIQUE
                " UNIQUE (" + COLUMN_RECHERCHE_ID + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_RECHERCHE_TABLE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_ARTEFACT_TABLE = "CREATE TABLE " + TABLE_NAME_ARTEFACT + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_NAME + " TEXT, " +
                COLUMN_ARTEFACT_ID + " TEXT, " +
                COLUMN_CATEGORIES + " TEXT, " +
                COLUMN_ARTEFACT_BRAND + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION + " TEXT, " +
                COLUMN_ARTEFACT_YEAR + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET + " TEXT, " +
                COLUMN_ARTEFACT_WORKING + " TEXT, " +
                COLUMN_ARTEFACT_BADGE + " TEXT, " +
                COLUMN_LAST_UPDATE+ " TEXT, " +

                // To assure the application have just one artefact entry per
                // artefact id, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_ARTEFACT_TABLE);
        //##############################################################################################
        final String SQL_CREATE_STATION_TRAVAIL_TABLE = "CREATE TABLE " + TABLE_NAME_STATION_TRAVAIL + " (" +
                _ID_STATION_TRAVAIL_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_STATION_TRAVAIL_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_STATION_TRAVAIL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_STATION_TRAVAIL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_STATION_TRAVAIL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_STATION_TRAVAIL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_STATION_TRAVAIL_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_STATION_TRAVAIL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_STATION_TRAVAIL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_STATION_TRAVAIL_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_STATION_TRAVAIL_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_STATION_TRAVAIL_TABLE + "," + COLUMN_CATEGORIES_STATION_TRAVAIL_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_STATION_TRAVAIL_TABLE);
        //##############################################################################################
        final String SQL_CREATE_ORDINATEUR_TABLE = "CREATE TABLE " + TABLE_NAME_ORDINATEUR + " (" +
                _ID_ORDINATEUR_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_ORDINATEUR_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_ORDINATEUR_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_ORDINATEUR_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_ORDINATEUR_TABLE + "," + COLUMN_CATEGORIES_ORDINATEUR_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_ORDINATEUR_TABLE);
        //##############################################################################################
        final String SQL_CREATE_ORDINATEUR_PORTABLE_TABLE = "CREATE TABLE " + TABLE_NAME_ORDINATEUR_PORTABLE + " (" +
                _ID_ORDINATEUR_PORTABLE_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_ORDINATEUR_PORTABLE_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_ORDINATEUR_PORTABLE_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_ORDINATEUR_PORTABLE_TABLE + "," + COLUMN_CATEGORIES_ORDINATEUR_PORTABLE_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_ORDINATEUR_PORTABLE_TABLE);
        //##############################################################################################
        final String SQL_CREATE_TERM_COM_TABLE = "CREATE TABLE " + TABLE_NAME_TERM_COM + " (" +
                _ID_TERM_COM_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_TERM_COM_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_TERM_COM_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_TERM_COM_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_TERM_COM_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_TERM_COM_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_TERM_COM_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_TERM_COM_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_TERM_COM_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_TERM_COM_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_TERM_COM_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_TERM_COM_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_TERM_COM_TABLE + "," + COLUMN_CATEGORIES_TERM_COM_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_TERM_COM_TABLE);
        //##############################################################################################
        final String SQL_CREATE_PERIPH_TABLE = "CREATE TABLE " + TABLE_NAME_PERIPH + " (" +
                _ID_PERIPH_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_PERIPH_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_PERIPH_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_PERIPH_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_PERIPH_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_PERIPH_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_PERIPH_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_PERIPH_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_PERIPH_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_PERIPH_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_PERIPH_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_PERIPH_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_PERIPH_TABLE + "," + COLUMN_CATEGORIES_PERIPH_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_PERIPH_TABLE);
        //##############################################################################################
        final String SQL_CREATE_COMPO_TABLE = "CREATE TABLE " + TABLE_NAME_COMPO + " (" +
                _ID_COMPO_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_COMPO_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_COMPO_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_COMPO_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_COMPO_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_COMPO_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_COMPO_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_COMPO_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_COMPO_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_COMPO_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_COMPO_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_COMPO_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_COMPO_TABLE + "," + COLUMN_CATEGORIES_COMPO_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_COMPO_TABLE);
        //##############################################################################################
        final String SQL_CREATE_SUPP_STOCKAGE_TABLE = "CREATE TABLE " + TABLE_NAME_SUPP_STOCKAGE + " (" +
                _ID_SUPP_STOCKAGE_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_SUPP_STOCKAGE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_SUPP_STOCKAGE_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_SUPP_STOCKAGE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_SUPP_STOCKAGE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_SUPP_STOCKAGE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_SUPP_STOCKAGE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_SUPP_STOCKAGE_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_SUPP_STOCKAGE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_SUPP_STOCKAGE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_SUPP_STOCKAGE_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_SUPP_STOCKAGE_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_SUPP_STOCKAGE_TABLE + "," + COLUMN_CATEGORIES_SUPP_STOCKAGE_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_SUPP_STOCKAGE_TABLE);
        //##############################################################################################
        final String SQL_CREATE_ORDI_POCHE_TABLE = "CREATE TABLE " + TABLE_NAME_ORDI_POCHE+ " (" +
                _ID_ORDI_POCHE_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_ORDI_POCHE_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_ORDI_POCHE_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_ORDI_POCHE_TABLE + "," + COLUMN_CATEGORIES_ORDI_POCHE_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_ORDI_POCHE_TABLE);
        //##############################################################################################
        final String SQL_CREATE_PROTOTYPE_TABLE = "CREATE TABLE " + TABLE_NAME_PROTOTYPE+ " (" +
                _ID_ORDI_POCHE_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_PROTOTYPE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_ORDI_POCHE_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_PROTOTYPE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_PROTOTYPE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_PROTOTYPE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_PROTOTYPE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_PROTOTYPE_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_PROTOTYPE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_PROTOTYPE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_PROTOTYPE_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_PROTOTYPE_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_PROTOTYPE_TABLE + "," + COLUMN_CATEGORIES_PROTOTYPE_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_PROTOTYPE_TABLE);
        //##############################################################################################
        final String SQL_CREATE_ORDI_BUREAU_TABLE = "CREATE TABLE " + TABLE_NAME_ORDI_BUREAU+ " (" +
                _ID_ORDI_BUREAU_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_ORDI_BUREAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_ORDI_BUREAU_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_ORDI_BUREAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_ORDI_BUREAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_ORDI_BUREAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_ORDI_BUREAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_ORDI_BUREAU_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_ORDI_BUREAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_ORDI_BUREAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_ORDI_BUREAU_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_ORDI_BUREAU_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_ORDI_BUREAU_TABLE + "," + COLUMN_CATEGORIES_ORDI_BUREAU_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_ORDI_BUREAU_TABLE);
        //##############################################################################################
        final String SQL_CREATE_SCSI_TABLE = "CREATE TABLE " + TABLE_NAME_SCSI+ " (" +
                _ID_SCSI_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_SCSI_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_SCSI_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_SCSI_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_SCSI_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_SCSI_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_SCSI_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_SCSI_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_SCSI_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_SCSI_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_SCSI_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_SCSI_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_SCSI_TABLE + "," + COLUMN_CATEGORIES_SCSI_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_SCSI_TABLE);
        //##############################################################################################
        final String SQL_CREATE_PHONE_TABLE = "CREATE TABLE " + TABLE_NAME_PHONE+ " (" +
                _ID_PHONE_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_PHONE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_PHONE_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_PHONE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_PHONE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_PHONE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_PHONE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_PHONE_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_PHONE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_PHONE_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_PHONE_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_PHONE_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_PHONE_TABLE + "," + COLUMN_CATEGORIES_PHONE_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_PHONE_TABLE);
        //##############################################################################################
        final String SQL_CREATE_REGLE_CALCUL_TABLE = "CREATE TABLE " + TABLE_NAME_REGLE_CALCUL+ " (" +
                _ID_REGLE_CALCUL_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_REGLE_CALCUL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_REGLE_CALCUL_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_REGLE_CALCUL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_REGLE_CALCUL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_REGLE_CALCUL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_REGLE_CALCUL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_REGLE_CALCUL_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_REGLE_CALCUL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_REGLE_CALCUL_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_REGLE_CALCUL_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_REGLE_CALCUL_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_REGLE_CALCUL_TABLE + "," + COLUMN_CATEGORIES_REGLE_CALCUL_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_REGLE_CALCUL_TABLE);
        //##############################################################################################
        final String SQL_CREATE_ECRAN_TABLE = "CREATE TABLE " + TABLE_NAME_ECRAN+ " (" +
                _ID_ECRAN_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_ECRAN_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_ECRAN_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_ECRAN_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_ECRAN_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_ECRAN_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_ECRAN_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_ECRAN_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_ECRAN_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_ECRAN_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_ECRAN_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_ECRAN_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_ECRAN_TABLE + "," + COLUMN_CATEGORIES_ECRAN_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_ECRAN_TABLE);
        //##############################################################################################
        final String SQL_CREATE_8_BITS_TABLE = "CREATE TABLE " + TABLE_NAME_8_BITS+ " (" +
                _ID_8_BITS_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_8_BITS_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_8_BITS_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_8_BITS_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_8_BITS_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_8_BITS_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_8_BITS_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_8_BITS_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_8_BITS_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_8_BITS_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_8_BITS_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_8_BITS_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_8_BITS_TABLE + "," + COLUMN_CATEGORIES_8_BITS_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_8_BITS_TABLE);
        //##############################################################################################
        final String SQL_CREATE_CABLE_ADAPTATEUR_TABLE = "CREATE TABLE " + TABLE_NAME_CABLE_ADAPTATEUR+ " (" +
                _ID_CABLE_ADAPTATEUR_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_CABLE_ADAPTATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_CABLE_ADAPTATEUR_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_CABLE_ADAPTATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_CABLE_ADAPTATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_CABLE_ADAPTATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_CABLE_ADAPTATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_CABLE_ADAPTATEUR_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_CABLE_ADAPTATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_CABLE_ADAPTATEUR_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_CABLE_ADAPTATEUR_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_CABLE_ADAPTATEUR_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_CABLE_ADAPTATEUR_TABLE + "," + COLUMN_CATEGORIES_CABLE_ADAPTATEUR_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_CABLE_ADAPTATEUR_TABLE);
        //##############################################################################################
        final String SQL_CREATE_RESEAU_TABLE = "CREATE TABLE " + TABLE_NAME_RESEAU+ " (" +
                _ID_RESEAU_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_RESEAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_NAME_RESEAU_TABLE + " TEXT, " +
                COLUMN_CATEGORIES_RESEAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BRAND_RESEAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_TIMEFRAME_RESEAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_DESCRIPTION_RESEAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_YEAR_RESEAU_TABLE + " INTEGER, " +
                COLUMN_ARTEFACT_TECDET_RESEAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_WORKING_RESEAU_TABLE + " TEXT, " +
                COLUMN_ARTEFACT_BADGE_RESEAU_TABLE + " TEXT, " +
                COLUMN_LAST_UPDATE_RESEAU_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_RESEAU_TABLE + "," + COLUMN_CATEGORIES_RESEAU_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_RESEAU_TABLE);
        //##############################################################################################
        final String SQL_CREATE_IMAGES_TABLE = "CREATE TABLE " + TABLE_NAME_IMAGES+ " (" +
                _ID_IMAGES_TABLE + " INTEGER PRIMARY KEY," +
                COLUMN_ARTEFACT_ID_IMAGES_TABLE + " TEXT, " +
                COLUMN_NAME_PICTURE_IMAGES_TABLE + " TEXT, " +
                COLUMN_DESCRIPTION_PICTURE_IMAGES_TABLE + " TEXT, " +

                // To assure the application have just one artefact/category pair entry per
                // artefact name and category, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ARTEFACT_ID_IMAGES_TABLE + "," + COLUMN_NAME_PICTURE_IMAGES_TABLE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_IMAGES_TABLE);
        //##############################################################################################
        final String SQL_CREATE_RECHERCHE_TABLE = "CREATE TABLE " + TABLE_NAME_RECHERCHE + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_RECHERCHE_NAME + " TEXT, " +
                COLUMN_RECHERCHE_ID + " TEXT, " +
                COLUMN_CATEGORIES_RECHERCHE_TABLE + " TEXT, " +
                COLUMN_RECHERCHE_BRAND + " TEXT, " +
                COLUMN_RECHERCHE_TIMEFRAME + " TEXT, " +
                COLUMN_RECHERCHE_DESCRIPTION + " TEXT, " +
                COLUMN_RECHERCHE_YEAR + " INTEGER, " +
                COLUMN_RECHERCHE_TECDET + " TEXT, " +
                COLUMN_RECHERCHE_WORKING + " TEXT, " +
                COLUMN_RECHERCHE_BADGE + " TEXT, " +
                COLUMN_LAST_UPDATE_RECHERCHE_TABLE+ " TEXT, " +

                // To assure the application have just one artefact entry per
                // artefact id, it's created a UNIQUE
                " UNIQUE (" + COLUMN_RECHERCHE_ID + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_RECHERCHE_TABLE);
        //##############################################################################################
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion != oldVersion) {
            db.execSQL("drop table if exists " + TABLE_NAME_ARTEFACT);
            db.execSQL("drop table if exists " + TABLE_NAME_STATION_TRAVAIL);
            db.execSQL("drop table if exists " + TABLE_NAME_ORDINATEUR);
            db.execSQL("drop table if exists " + TABLE_NAME_ORDINATEUR_PORTABLE);
            db.execSQL("drop table if exists " + TABLE_NAME_TERM_COM);
            db.execSQL("drop table if exists " + TABLE_NAME_PERIPH);
            db.execSQL("drop table if exists " + TABLE_NAME_COMPO);
            db.execSQL("drop table if exists " + TABLE_NAME_SUPP_STOCKAGE);
            db.execSQL("drop table if exists " + TABLE_NAME_ORDI_POCHE);
            db.execSQL("drop table if exists " + TABLE_NAME_PROTOTYPE);
            db.execSQL("drop table if exists " + TABLE_NAME_ORDI_BUREAU);
            db.execSQL("drop table if exists " + TABLE_NAME_SCSI);
            db.execSQL("drop table if exists " + TABLE_NAME_PHONE);
            db.execSQL("drop table if exists " + TABLE_NAME_REGLE_CALCUL);
            db.execSQL("drop table if exists " + TABLE_NAME_ECRAN);
            db.execSQL("drop table if exists " + TABLE_NAME_8_BITS);
            db.execSQL("drop table if exists " + TABLE_NAME_CABLE_ADAPTATEUR);
            db.execSQL("drop table if exists " + TABLE_NAME_RESEAU);
            db.execSQL("drop table if exists " + TABLE_NAME_IMAGES);
            db.execSQL("drop table if exists " + TABLE_NAME_RECHERCHE);

            onCreate(db);
        }
    }

    /**
     * Fills ContentValues result from a Artefact object
     */
    private ContentValues fill(Artefact artefact) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ARTEFACT_NAME, artefact.getName());
        values.put(COLUMN_ARTEFACT_ID, artefact.getIdArtefact());
        values.put(COLUMN_CATEGORIES, artefact.getCategory());
        values.put(COLUMN_ARTEFACT_BRAND, artefact.getBrand());
        values.put(COLUMN_ARTEFACT_TIMEFRAME, artefact.getTimeFrame());
        values.put(COLUMN_ARTEFACT_DESCRIPTION, artefact.getDescription());
        values.put(COLUMN_ARTEFACT_YEAR, artefact.getYear());
        values.put(COLUMN_ARTEFACT_TECDET, artefact.getTecDetails());
        values.put(COLUMN_ARTEFACT_WORKING, artefact.getWorking());
        values.put(COLUMN_ARTEFACT_BADGE, artefact.getBadge());
        values.put(COLUMN_LAST_UPDATE, artefact.getLastUpdate());
        return values;
    }

    /**
     * Adds a new artefact
     * @return  true if the artefact was added to the table ; false otherwise
     */
    public boolean addArtefact(Artefact artefact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(artefact);

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (team, championship)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME_ARTEFACT, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    public boolean addArtefactToCateoriesTable(Artefact artefact, String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(artefact);

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (team, championship)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(tableName, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * ajoute le nom, les images et leur description d'un item a la table Images
     *
     * @param artefactName
     * @param imgName
     * @param imgDescription
     * @return
     */
    public boolean addPictureToImagesTable(String artefactName, String imgName, String imgDescription) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ARTEFACT_ID_IMAGES_TABLE, artefactName);
        values.put(COLUMN_NAME_PICTURE_IMAGES_TABLE, imgName);
        values.put(COLUMN_DESCRIPTION_PICTURE_IMAGES_TABLE, imgDescription);

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (team, championship)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME_IMAGES, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Returns a cursor on all the artefacts of the data base
     */
    public Cursor fetchAllArtefacts() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME_ARTEFACT, null,
                null, null, null, null, COLUMN_ARTEFACT_NAME +" ASC", null);

        Log.d(TAG, "call fetchAllArtefacts()");
        if (cursor != null) { cursor.moveToFirst(); }
        return cursor;
    }

    /**
     * Returns a list on all the artefacts of the data base
     */
    public List<Artefact> getAllArtefacts() {
        List<Artefact> res = new ArrayList<>();
        final Cursor cursor = fetchAllArtefacts();
        if(cursor !=null) {
            cursor.moveToFirst();
            res.add(cursorToArtefact(cursor));
        }
        while(cursor.moveToNext()) { res.add(cursorToArtefact(cursor)); }
        return res;
    }

    /**
     * cursor sur un item
     *
     * @param cursor
     * @return
     */
    public Artefact cursorToArtefact(Cursor cursor) {

        Artefact artefact = new Artefact(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORIES)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_BRAND)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_TIMEFRAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_DESCRIPTION)),
                cursor.getInt(cursor.getColumnIndex(COLUMN_ARTEFACT_YEAR)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_TECDET)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_WORKING)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ARTEFACT_BADGE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_LAST_UPDATE))
        );
        return artefact;
    }

    /**
     * Returns a cursor on all the artefacts of the data base order by date
     */
    public Cursor fetchAllArtefactsOrderByDate() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME_ARTEFACT, null,
                null, null, null, null, COLUMN_ARTEFACT_YEAR +" ASC", null);

        Log.d(TAG, "call fetchAllArtefacts()");
        if (cursor != null) { cursor.moveToFirst(); }
        return cursor;
    }

    /**
     * return tous les item correspond à la catégorie "tableName"
     */
    public Cursor fetchArtefactsByCat(String tableName) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + tableName , null);

        Log.d(TAG, "call fetchByCatArtefacts()");
        if (cursor != null) { cursor.moveToFirst(); }
        return cursor;
    }

    /**
     * return la colonne imgName de la table Images en fonction d'un cursor
     *
     * @param cursor
     * @return
     */
    public String cursorToPicture(Cursor cursor) {
        return(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_PICTURE_IMAGES_TABLE)));
    }

    /**
     * return la colonne description de la table Images en fonction d'un cursor
     *
     * @param cursor
     * @return
     */
    public String cursorToPictureDescription(Cursor cursor) {
        return(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION_PICTURE_IMAGES_TABLE)));
    }

    /**
     * return une liste qui contient toutes les images d'un item
     *
     * @return
     */
    public List<String> getAllPicturesNameOfItem() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<String> listPicture = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT "+COLUMN_NAME_PICTURE_IMAGES_TABLE+" FROM "+TABLE_NAME_IMAGES ,null);
        if(cursor !=null) {
            if(cursor.moveToFirst()) {
                cursor.moveToFirst();
                listPicture.add(cursorToPicture(cursor));
            }
        }
        while(cursor.moveToNext()) { listPicture.add(cursorToPicture(cursor)); }
        return listPicture;
    }

    /**
     * return la description d'une image
     *
     * @param imgName
     * @return
     */
    public String getAllPicturesDescriptionOfItem(String imgName) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<String> listDescription = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT "+COLUMN_DESCRIPTION_PICTURE_IMAGES_TABLE+" FROM "+TABLE_NAME_IMAGES+" WHERE "+COLUMN_NAME_PICTURE_IMAGES_TABLE+" LIKE ?" , new String[]{imgName});
        if(cursor !=null) {
            if(cursor.moveToFirst()) {
                cursor.moveToFirst();
                listDescription.add(cursorToPictureDescription(cursor));
            }
        }
        while(cursor.moveToNext()) { listDescription.add(cursorToPictureDescription(cursor)); }
        return listDescription.get(0);
    }

    /**
     * remplit la table Recherche en fonction d'une string
     *
     * @param queryParam
     */
    private void fillTableRecherche(String queryParam) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Artefact> listArtefactPourLaRecherche = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME_ARTEFACT+" WHERE ("+COLUMN_ARTEFACT_NAME+" LIKE ?) " +
                "OR ("+COLUMN_ARTEFACT_BRAND+" LIKE ?) " +
                "OR ("+COLUMN_CATEGORIES+" LIKE ?) " +
                "OR ("+COLUMN_ARTEFACT_ID+" LIKE ?) " +
                "OR ("+COLUMN_ARTEFACT_TIMEFRAME+" LIKE ?) " +
                "OR ("+COLUMN_ARTEFACT_DESCRIPTION+" LIKE ?) " +
                "OR ("+COLUMN_ARTEFACT_YEAR+" LIKE ?) " +
                "OR ("+COLUMN_ARTEFACT_TECDET+" LIKE ?) " +
                "OR ("+COLUMN_ARTEFACT_WORKING+" LIKE ?) " + "OR ("+COLUMN_ARTEFACT_WORKING+" LIKE ?) ", new String[]{queryParam,queryParam,queryParam,queryParam,queryParam,queryParam,queryParam,queryParam,queryParam});
        if(cursor !=null) {
            if(cursor.moveToFirst()) {
                cursor.moveToFirst();
                listArtefactPourLaRecherche.add(cursorToArtefact(cursor));
            }
        }
        while(cursor.moveToNext()) { listArtefactPourLaRecherche.add(cursorToArtefact(cursor)); }
        for(int i=0 ; i<listArtefactPourLaRecherche.size() ; i++){
            addArtefactToRechercheTable(listArtefactPourLaRecherche.get(i));
        }
    }

    /**
     * ajoute un item a la table Recherche
     *
     * @param artefact
     * @return
     */
    public boolean addArtefactToRechercheTable(Artefact artefact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = fill(artefact);

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (team, championship)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME_RECHERCHE, null, values, CONFLICT_IGNORE);

        return (rowID != -1);
    }

    /**
     * cursor sur tous les elements de la table recherche
     *
     * @param recherche
     * @return
     */
    public Cursor fetchResultOfSearch(String recherche) {
        SQLiteDatabase db = this.getReadableDatabase();
        String queryParam = "%"+recherche+"%";
        fillTableRecherche(queryParam);
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME_RECHERCHE, null);

        Log.d(TAG, "call fetchResultOfSearch()");
        if (cursor != null) { cursor.moveToFirst(); }
        return cursor;
    }
}
