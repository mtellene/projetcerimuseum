package com.example.projetappmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rv;

    private List<Artefact> listArtefact;

    private ArtefactDbHelper dbHelper;

    public MyAdapter adapter;

    private EditText searchBar;

    private SQLiteDatabase db;

    private Cursor lastCursor;  //recupere le dernier cursor utilise


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);

        dbHelper = new ArtefactDbHelper(this);
        db = dbHelper.getReadableDatabase();

        Cursor mCursorArtefact = db.rawQuery("SELECT * FROM " + dbHelper.TABLE_NAME_ARTEFACT, null);
        boolean asyncTask = false;
        if(mCursorArtefact.getCount() == 0) {   //si la table artefact de la bdd est vide
            new AsyncTaskRecup().execute(listArtefact);
            asyncTask = true;
        }

        searchBar = (EditText) findViewById(R.id.searchBar);

        rv = (RecyclerView) findViewById(R.id.recyclerViewArtefact);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        if(!asyncTask){
            Cursor cursorAlpha = dbHelper.fetchAllArtefacts();
            lastCursor = cursorAlpha;
            adapter = new MyAdapter(MainActivity.this, cursorAlpha);
            rv.setAdapter(adapter);
        }

        final Button butSearch = (Button) findViewById(R.id.buttonSearch);
        butSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchBar.getText().length() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertDialogBuilder.setTitle("Erreur de recherche");
                    alertDialogBuilder.setMessage("Vous devez entrer une recherche");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }else{
                    dbHelper.dropRechercheTable(db);
                    Cursor cursorSearch = dbHelper.fetchResultOfSearch(searchBar.getText().toString());
                    Cursor countBdd = db.rawQuery("SELECT * FROM "+ArtefactDbHelper.TABLE_NAME_RECHERCHE,null);
                    if(countBdd !=null) {
                        if(!countBdd.moveToFirst()) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            alertDialogBuilder.setTitle("Erreur de recherche");
                            alertDialogBuilder.setMessage("Aucun résultat trouvé");
                            alertDialogBuilder.create();
                            alertDialogBuilder.show();
                            adapter = new MyAdapter(MainActivity.this, lastCursor);
                            rv.setAdapter(adapter);
                        }else{
                            adapter = new MyAdapter(MainActivity.this, cursorSearch);
                            rv.setAdapter(adapter);
                            lastCursor = cursorSearch;
                        }
                    }
                }
            }
        });

        final Button butOrdreAlpha = (Button) findViewById(R.id.buttonOrdreAlpha);
        butOrdreAlpha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor cursorAlpha = dbHelper.fetchAllArtefacts();
                adapter = new MyAdapter(MainActivity.this, cursorAlpha);
                rv.setAdapter(adapter);
                lastCursor = cursorAlpha;
            }
        });

        final Button butOrdreChrono = (Button) findViewById(R.id.buttonOrdreChrono);
        butOrdreChrono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor cursorChrono = dbHelper.fetchAllArtefactsOrderByDate();
                adapter = new MyAdapter(MainActivity.this, cursorChrono);
                rv.setAdapter(adapter);
                lastCursor = cursorChrono;
            }
        });

        final Button butOrdreCat = (Button) findViewById(R.id.buttonCategory);
        butOrdreCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CategoryActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    class MyAdapter extends RecyclerView.Adapter<RowHolder> {

        public CursorAdapter cursorFromDb;
        public Context context;

        public MyAdapter(Context context, Cursor cursor){
            this.context = context;
            cursorFromDb = new CursorAdapter(context, cursor, 0) {
                @Override
                public View newView(Context context, Cursor cursor, ViewGroup parent) {
                    return null;
                }

                @Override
                public void bindView(View view, Context context, Cursor cursor) {

                }
            };
        }

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.row_artefact, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            cursorFromDb.getCursor().moveToPosition(position);
            holder.bindModel(cursorFromDb.getCursor());
        }

        @Override
        public int getItemCount() {
            return (cursorFromDb.getCount());
        }

    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView nameArtefact;
        TextView catArtefact;
        ImageView img;

        private Artefact artefact;

        long rowID = 0;

        RowHolder(View row) {
            super(row);
            row.setOnClickListener(this);
            nameArtefact = (TextView) row.findViewById(R.id.nameArtefact);
            catArtefact = (TextView) row.findViewById(R.id.catArtefact);
            img = (ImageView) row.findViewById(R.id.imgArtefact);
        }

        @Override
        public void onClick(View v) {
            System.out.println(artefact.getName());
            Intent intent = new Intent(MainActivity.this, ArtefactActivity.class);
            intent.putExtra(Artefact.TAG, artefact);
            startActivityForResult(intent, 2);
        }

        void bindModel(Cursor cursor) {
            artefact = dbHelper.cursorToArtefact(cursor);
            rowID = artefact.getId();
            nameArtefact.setText(artefact.getName());
            catArtefact.setText(artefact.getCategory());
            File path = new File(MainActivity.this.getExternalFilesDir(null), cursor.getString(cursor.getColumnIndex(dbHelper.COLUMN_ARTEFACT_NAME))+".png");
            Bitmap myBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
            img.setImageBitmap(myBitmap);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * ici on recupere tous les items du musee
     */
    private class AsyncTaskRecup extends AsyncTask<List<Artefact>, String, List<Artefact>>{

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Téléchargement des objets...");
            progressDialog.show();
        }

        @Override
        protected List<Artefact> doInBackground(List<Artefact>... lists) {
            URL url = null;
            HttpURLConnection urlConnection = null;
            try {
                url = WebServiceUrl.buildSearchAllArtefacts();
                urlConnection = (HttpURLConnection) url.openConnection();
                JsonCatalog jsonCatalog = new JsonCatalog(dbHelper);
                try {
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                    jsonCatalog.readJsonStream(is);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) { //for buildSearchTeam
                e.printStackTrace();
            } catch (IOException e) {   //for openConnection
                e.printStackTrace();
            }
            return listArtefact;
        }

        protected void onPostExecute(List<Artefact> listartefact){
            super.onPostExecute(listartefact);
            progressDialog.dismiss();
            new AsyncTaskDlBadge().execute();
        }
    }

    /**
     * ici on telecharge dans le stockage interne tous les badges des items du musee
     */
    private class AsyncTaskDlBadge extends AsyncTask<List<Artefact>, String, List<Artefact>> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Téléchargement des badges des objets...");
            progressDialog.show();
        }

        @Override
        protected List<Artefact> doInBackground(List<Artefact>... lists) {
            listArtefact = dbHelper.getAllArtefacts();
            for(Artefact artefact : listArtefact){
                saveToInternalStorage(getImageFromURL(artefact.getBadge()), artefact.getName());
            }
            return listArtefact;
        }

        @Override
        protected void onPostExecute(List<Artefact> artefacts) {
            super.onPostExecute(artefacts);
            progressDialog.dismiss();
            adapter = new MyAdapter(MainActivity.this, dbHelper.fetchAllArtefacts());
            lastCursor = dbHelper.fetchAllArtefacts();
            rv.setAdapter(adapter);
        }
    }

    /**
     * gestion du lien ou est le badge a telecharger
     *
     * @param urlString
     * @return
     */
    public Bitmap getImageFromURL(String urlString)
    {
        Bitmap bm = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL url2 = new URL(urlString);
            httpURLConnection = (HttpURLConnection) url2.openConnection();
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            bm = BitmapFactory.decodeStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
        }
        return bm;
    }

    /**
     * sauvegarde au format png dans le stockage interne
     *
     * @param bm
     * @param name
     */
    private void saveToInternalStorage(Bitmap bm, String name)
    {
        File pathToBadge = new File(this.getExternalFilesDir(null), name+".png");
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(pathToBadge);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bm.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
