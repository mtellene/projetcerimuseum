package com.example.projetappmobile;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JsonItemPictures {

    private ArtefactDbHelper dbHelper;

    private Artefact artefact;

    private List<String> pictures;
    private List<String> description;

    public JsonItemPictures(ArtefactDbHelper dbHelper, Artefact artefact) {
        this.dbHelper = dbHelper;
        this.artefact = artefact;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readItem(reader);
        } finally {
            reader.close();
        }
    }

    public void readItem(JsonReader reader) throws IOException {
        reader.beginObject();   //asserts it is the begin of the reader
        readItemArray(reader,artefact); //call readCatalogArray on the reader
        reader.endObject(); //asserts it is the end of the reader
    }

    public void readItemArray(JsonReader reader, Artefact artefact) throws IOException {
        pictures = new ArrayList<>();
        description = new ArrayList<>();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if(name.equals("pictures")){  //recup pictures
                reader.beginObject();    //c'est un objet json
                while (reader.hasNext()){
                    String nameImg = reader.nextName();
                    pictures.add(nameImg);
                    String descImg = reader.nextString();
                    description.add(descImg);
                }
                reader.endObject();
            } else {
                reader.skipValue();
            }
        }
        //gestion des images
        for(int i=0 ; i<pictures.size() ; i++){
            dbHelper.addPictureToImagesTable(artefact.getIdArtefact(), pictures.get(i), description.get(i));
        }
    }
}
