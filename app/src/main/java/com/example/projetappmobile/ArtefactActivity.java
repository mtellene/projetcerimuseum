package com.example.projetappmobile;

import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class ArtefactActivity extends AppCompatActivity{

    private TextView textArtefactName, textCategories, textBrand, textYear, textTimeFrame, textWorking, textTecDet, textDescription, textLastUpdate;
    private TextView textDescriptionImgBas;

    private ImageView imageArtefact, imgArtefactEnBas;

    private Artefact artefact;

    private ArtefactDbHelper dbHelper;

    private int imgId = 0;
    private int maxId = 0;

    private List<String> listPictures;

    private Button butImgSuivante;

    private String descriptionImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artefact);

        dbHelper = new ArtefactDbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        dbHelper.dropImgTable(db);
        artefact = (Artefact) getIntent().getParcelableExtra(Artefact.TAG);

        textArtefactName = (TextView) findViewById(R.id.nameArtefact);
        textCategories = (TextView) findViewById(R.id.categories);
        textBrand = (TextView) findViewById(R.id.editBrand);
        textYear = (TextView) findViewById(R.id.editYear);
        textTimeFrame = (TextView) findViewById(R.id.editTimeFrame);
        textWorking = (TextView) findViewById(R.id.editWorking);
        textTecDet = (TextView) findViewById(R.id.editTecDet);
        textDescription = (TextView) findViewById(R.id.editDescription);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageArtefact = (ImageView) findViewById(R.id.imgArtefact);
        imgArtefactEnBas = (ImageView) findViewById(R.id.grandeImgArtefact);

        textDescriptionImgBas = (TextView) findViewById(R.id.labelDescrptionImg);
        
        updateView();

        butImgSuivante = (Button) findViewById(R.id.buttonChangerImg);
        butImgSuivante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTaskLoadImgBot().execute();
                imgId = imgId + 1;
            }
        });
    }

    private void updateView() {
        textArtefactName.setText(artefact.getName());
        textCategories.setText(artefact.getCategory());
        textBrand.setText(artefact.getBrand());
        textYear.setText(String.valueOf(artefact.getYear()));
        textTimeFrame.setText(artefact.getTimeFrame());
        textWorking.setText(artefact.getWorking());
        textTecDet.setText(artefact.getTecDetails());
        textDescription.setText(artefact.getDescription());
        textLastUpdate.setText(artefact.getLastUpdate());

        new AsyncTaskBddImg().execute();
    }

    /**
     * ici on recupere toutes les images lié à un item
     */
    private class AsyncTaskBddImg extends AsyncTask<Artefact,Void, Artefact> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(ArtefactActivity.this);
            progressDialog.setMessage("Téléchargement des images...");
            progressDialog.show();
        }

        @Override
        protected Artefact doInBackground(Artefact... artefacts) {
            URL url = null;
            HttpURLConnection urlConnection = null;
            try {
                url = WebServiceUrl.buildSearchPicturesArtefact(artefact.getIdArtefact());
                urlConnection = (HttpURLConnection) url.openConnection();
                JsonItemPictures jsonItemPictures = new JsonItemPictures(dbHelper,artefact);
                try {
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                    jsonItemPictures.readJsonStream(is);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {   //for openConnection
                e.printStackTrace();
            }
            return artefact;
        }

        protected void onPostExecute(Artefact artefact){
            super.onPostExecute(artefact);
            progressDialog.dismiss();
            new AsyncTaskImage().execute();
            listPictures = dbHelper.getAllPicturesNameOfItem();
            maxId = listPictures.size();
        }
    }

    /**
     * ici on affiche le badge d'un item
     */
    private class AsyncTaskImage extends AsyncTask<String,Void, Bitmap> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(ArtefactActivity.this);
            progressDialog.setMessage("Mise en place du badge...");
            progressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap = null;
            try{
                URL url = new URL(artefact.getBadge());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                    bitmap = BitmapFactory.decodeStream(is);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bm){
            super.onPostExecute(bm);
            progressDialog.dismiss();
            imageArtefact.setImageBitmap(bm);
            if(listPictures.size() != 0) {
                new AsyncTaskLoadImgBot().execute();
            }else{
                imgArtefactEnBas.setVisibility(View.INVISIBLE);
                textDescriptionImgBas.setText("Pas d'images disponibles");
                butImgSuivante.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * ici on va affiche l'image en bas (appeler au debut et a chaque click sur le bouton image suivante)
     * si on a parcouru toutes les images, on re-affiche la premiere
     */
    private class AsyncTaskLoadImgBot extends AsyncTask<String,Void, Bitmap> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(ArtefactActivity.this);
            progressDialog.setMessage("Mise en place de l'image...");
            progressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap = null;
            try{
                if(imgId >= maxId){ imgId = 0; }
                String imgName = listPictures.get(imgId);
                descriptionImg = dbHelper.getAllPicturesDescriptionOfItem(imgName);
                URL url = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+artefact.getIdArtefact()+"/images/"+imgName);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                    bitmap = BitmapFactory.decodeStream(is);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bm){
            super.onPostExecute(bm);
            progressDialog.dismiss();
            textDescriptionImgBas.setText(descriptionImg);
            imgArtefactEnBas.setImageBitmap(bm);
        }
    }
}
